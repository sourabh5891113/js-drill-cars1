// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical
//order and log the results in the console as it was returned.
let inventory = require("../data");
let sortCarModels = require("../problems/problem3");

sortCarModels(inventory);
