function getCarById(inventory, id) {
  let founded = false;
  if (Array.isArray(inventory) && id !== undefined && typeof id === "number") {
    for (let car of inventory) {
      if (car.id === id) {
        console.log(
          `Car ${car.id} is a ${car.car_year} ${car.car_make} ${car.car_model} `
        );
        founded = true;
      }
    }
    if (!founded) {
      console.log("No car with that ID exist");
    }
  } else {
    console.log(
      "First argument should be an array and please add an ID which should be a number"
    );
  }
}

module.exports = getCarById;
