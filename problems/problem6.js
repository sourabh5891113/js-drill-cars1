function getBMWandAudiCars(inventory) {
  if (Array.isArray(inventory)) {
    let BMWAndAudiArray = [];
    for (let car of inventory) {
      if (car.car_make === "Audi" || car.car_make === "BMW") {
        BMWAndAudiArray.push(car);
      }
    }
    console.log(JSON.stringify(BMWAndAudiArray));
  } else {
    console.log("First argument should be the inventory");
  }
}

module.exports = getBMWandAudiCars;
