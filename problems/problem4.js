function getCarYears(inventory) {
  if (Array.isArray(inventory)) {
    let carYearsArray = [];
    for (let car of inventory) {
      carYearsArray.push(car.car_year);
    }
    return carYearsArray;
  } else {
    console.log("First argument should be the inventory");
    return [];
  }
}

module.exports = getCarYears;
