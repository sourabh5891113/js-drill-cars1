let getCarYears = require("./problem4");
function getCarsOlderThen2000(inventory) {
  if (Array.isArray(inventory)) {
    let carYearsArray = getCarYears(inventory);
    let olderCars = [];
    for (let year of carYearsArray) {
      if (year < 2000) olderCars.push(year);
    }
    console.log(olderCars);
    console.log(
      `there are ${olderCars.length} cars that were made before the year 2000`
    );

    return olderCars;
  } else {
    console.log("First argument should be the inventory");
  }
}

module.exports = getCarsOlderThen2000;
