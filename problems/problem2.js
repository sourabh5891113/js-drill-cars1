function getLastCar(inventory) {
  if (Array.isArray(inventory)) {
    const totalItems = inventory.length;
    const lastCar = inventory[totalItems - 1];
    console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);
  } else {
    console.log("First argument should be the inventory");
  }
}

module.exports = getLastCar;
