function swapToElements(carModelsArray, element1, element2) {
  //swapping two elements
  let temp = carModelsArray[element1];
  carModelsArray[element1] = carModelsArray[element2];
  carModelsArray[element2] = temp;
}

function selection_sort(carModelsArray) {
  let length = carModelsArray.length;

  for (let index1 = 0; index1 < length - 1; index1++) {
    let smallestModel = index1;
    for (let index2 = index1 + 1; index2 < length; index2++) {
      if (carModelsArray[index2] < carModelsArray[smallestModel]) {
        smallestModel = index2;
      }
    }
    if (smallestModel !== index1) {
      swapToElements(carModelsArray, index1, smallestModel); //putting the lexicographically smaller car model at first
    }
  }
}

function sortCarModels(inventory) {
  if (Array.isArray(inventory)) {
    let carModelsArray = [];
    for (let car of inventory) {
      carModelsArray.push(car.car_model.toLowerCase());
    }
    selection_sort(carModelsArray);
    console.log(carModelsArray);
  } else {
    console.log("First argument should be the inventory");
  }
}

module.exports = sortCarModels;
